package com.mysite.websocket.mapper;

import com.mysite.websocket.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {

    UserVo getUser(String username);

    List<UserVo> getUsers(Map<String, Object> searchMap);
}
