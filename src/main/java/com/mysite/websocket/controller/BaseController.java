package com.mysite.websocket.controller;

import org.springframework.beans.factory.annotation.Value;

public abstract class BaseController {

    @Value("${cust.storage.file.path}")
    protected String storageFilePath;
}
