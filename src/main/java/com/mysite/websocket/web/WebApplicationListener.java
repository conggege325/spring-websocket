package com.mysite.websocket.web;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class WebApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${cust.storage.file.path}")
    private String storageFilePath;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        this.createDirectory(storageFilePath);
    }

    private void createDirectory(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
