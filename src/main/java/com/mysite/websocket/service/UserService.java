package com.mysite.websocket.service;

import com.mysite.websocket.mapper.UserMapper;
import com.mysite.websocket.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public UserVo findUser(String username, String password) {
        UserVo vo = userMapper.getUser(username);
        if(vo != null && password.equals(vo.getPassword())) {
            vo.setPassword(null);
            return vo;
        }
        return null;
    }
}
