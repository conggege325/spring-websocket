package com.mysite.websocket.vo;

public enum WebsocketMessageType {
	TEXT, TEXT_SYS, BINARY;
}
