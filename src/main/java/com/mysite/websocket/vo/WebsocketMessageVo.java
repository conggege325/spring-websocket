package com.mysite.websocket.vo;

import java.util.Date;

public class WebsocketMessageVo {

	private WebsocketMessageType type = WebsocketMessageType.TEXT;

	private Object content;

	private String speaker;
	
	private Date time;

	public WebsocketMessageVo() {
	}

	public WebsocketMessageVo(String speaker, Object content) {
	    this.speaker = speaker;
		this.content = content;
	}

	public WebsocketMessageVo(WebsocketMessageType type, Object content) {
		this.type = type;
		this.content = content;
	}

	public WebsocketMessageType getType() {
		return type;
	}

	public void setType(WebsocketMessageType type) {
		this.type = type;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public Date getTime() {
        return new Date();
    }
}
