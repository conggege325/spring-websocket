package com.mysite.websocket.vo;

public class FileVo {

    private String name;
    private String contentType;
    private String path;
    private String md5;
    private Long size;

    public FileVo() {
    }

    public FileVo(String name, String contentType, String path, String md5, Long size) {
        this.name = name;
        this.contentType = contentType;
        this.path = path;
        this.md5 = md5;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
