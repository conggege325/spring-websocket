package com.mysite.websocket.util;

import com.alibaba.fastjson.JSONObject;

public class JsonUtil {

	public static String toJson(Object object) {
		return JSONObject.toJSONStringWithDateFormat(object, "yyyy-MM-dd HH:mm:ss");
	}
}
