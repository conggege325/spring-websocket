<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>websocket test</title>
</head>
<body>
<script src="http://cdn.bootcss.com/jquery/3.1.0/jquery.min.js"></script>
<script>
  function login(event) {
    if (event && event.keyCode != 13) return;
    $.ajax({
      type: "POST",
      url: "/login",
      data: {
        username: $("#username").val(),
        password: $("#password").val()
      },
      dataType: "json",
      success: function (data) {
        if (data.logined)
          location.href = 'index'
        else
          alert('username or password error !')
      }
    });
  }

  $(function () {
    $("#username").focus();
  })
</script>

<div>
  <label>username:</label><input id="username" value="admin" onkeyup="login(event)"/>
</div>
<div>
  <label>password:</label><input id="password" type="password" value="1" onkeyup="login(event)"/>
</div>
<div>
  <button id="loginbtn" onclick="login()">login</button>
</div>
</body>
</html>