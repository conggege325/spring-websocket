<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <title>websocket在线聊天室</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

  <!-- jquery -->
  <script src="http://cdn.bootcss.com/jquery/3.1.0/jquery.min.js"></script>

  <!-- easyui 布局 -->
  <link rel="stylesheet" type="text/css" href="./plugins/jquery-easyui-1.6.2/themes/material-teal/layout.css">
  <link rel="stylesheet" type="text/css" href="./plugins/jquery-easyui-1.6.2/themes/material-teal/panel.css">
  <script type="text/javascript" src="./plugins/jquery-easyui-1.6.2/plugins/jquery.resizable.js"></script>
  <script type="text/javascript" src="./plugins/jquery-easyui-1.6.2/plugins/jquery.parser.js"></script>
  <script type="text/javascript" src="./plugins/jquery-easyui-1.6.2/plugins/jquery.panel.js"></script>
  <script type="text/javascript" src="./plugins/jquery-easyui-1.6.2/plugins/jquery.layout.js"></script>

  <!-- websocket -->
  <script src="http://cdn.bootcss.com/sockjs-client/1.1.1/sockjs.js"></script>
  
  <!-- 文件上传 -->
  <script type="text/javascript" src="./js/uploader.js"></script>

  <!-- 自定义 -->
  <link rel="stylesheet" href="./css/style.css">
  <script>window.CURRENT_USER = JSON.parse('${CURRENT_USER}');</script>
  <script src="./js/websocket.js"></script>
</head>

<body class="easyui-layout">

<div data-options="region:'north'" class="nav right">
  <span>Hello,</span>
  <span id="current_username"></span>
  <a href="./logout">登出</a>
</div>

<div data-options="region:'west',split:true" title="在线用户列表" class="menu">

</div>

<div data-options="region:'center',border:false">
  <div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center',border:false" id="shown" class="contents"></div>

    <div data-options="region:'south',border:false,split:true" class="footer">
      <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north',border:false">
          <div class="tools">
            <a id="uploader" onclick="uploadFile()"><img src="./images/file.svg"></a>
          </div>
        </div>
        <div data-options="region:'center',border:false" id="editor" contenteditable="true"></div>
        <div data-options="region:'south',border:false">
          <div class="send">
            <button id="send" disabled="disabled" onclick="sendMessage()">发送</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
